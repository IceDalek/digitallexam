create table IF NOT EXISTS students(
                                       id int PRIMARY key,
                                       name varchar (40),
                                       lastName varchar (40),
                                       subject varchar(40),
                                       course int

);
create table IF NOT EXISTS teachers(
                                       id int PRIMARY key,
                                       name varchar (40),
                                       lastName varchar (40),
                                       subject varchar (40)
);
create table IF NOT EXISTS students_teachers(
                                                student_id int NOT NULL,
                                                teacher_id int NOT NULL,
                                                PRIMARY KEY (student_id, teacher_id),
                                                FOREIGN KEY (teacher_id) REFERENCES teachers(id) ON UPDATE CASCADE,
                                                FOREIGN KEY (student_id) REFERENCES students(id) ON UPDATE CASCADE
);
create table accounts(
    username varchar(50) primary key not null ,
    password text
    )
    insert into accounts(username, password) VALUES ()