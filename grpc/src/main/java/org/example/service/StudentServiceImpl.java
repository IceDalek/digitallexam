package org.example.service;

import com.example.grpc.StudentsServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.stub.StreamObserver;
import org.example.amqp.InfoPublisher;
import org.example.mybatis.entity.StudentEntity;

import org.example.mybatis.service.StudentService;

import java.util.List;

public class StudentServiceImpl extends StudentsServiceGrpc.StudentsServiceImplBase {
    private void sendMessageToLog(String message) {
        Thread brokerThread = new Thread(new InfoPublisher(message));
        brokerThread.setDaemon(false);
        brokerThread.start();
    }

    private TestService.GetStudentsByIdResponse buildGetStudByIdResponseFromEntity(StudentEntity studentEntity, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        TestService.GetStudentsByIdResponse response
                = TestService.GetStudentsByIdResponse.newBuilder()
                .setId((int) studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();
        return response;
    }

    private StudentService studentService = new StudentService();

    @Override
    public void getStudentById(TestService.GetStudentByIdRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        System.out.println(request);

        StudentEntity studentEntity = studentService.getStudentById(request.getId());
        TestService.GetStudentsByIdResponse response =
                buildGetStudByIdResponseFromEntity(studentEntity, responseObserver);
        responseObserver.onNext(response);
        System.out.println(response);
        sendMessageToLog("get student by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void getAllStudents(TestService.GetAllStudentsRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {

        List<StudentEntity> studentsList = studentService.getAllStudents();
        //will return response iterator
        for (StudentEntity studentEntity : studentsList) {
            TestService.GetStudentsByIdResponse response =
                    buildGetStudByIdResponseFromEntity(studentEntity, responseObserver);
            responseObserver.onNext(response);
            sendMessageToLog("get all students received" + request + " and sent response " + response);
            //System.out.println(response);
        }
        responseObserver.onCompleted();
    }

    @Override
    public void createNewStudent(TestService.AddNewStudentRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        System.out.println("request is " + request);
        System.out.println(request.getId());
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(request.getId());
        studentEntity.setName(request.getName());
        studentEntity.setLastName(request.getLastname());
        studentEntity.setSubject(request.getSubject());
        studentEntity.setCourse(request.getCourse());
        studentService.createNewStudent(studentEntity);
        System.out.println("add new student");
        TestService.GetStudentsByIdResponse response =
                buildGetStudByIdResponseFromEntity(studentEntity, responseObserver);
        responseObserver.onNext(response);
        sendMessageToLog("create new student received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void updateStudentById(TestService.UpdateStudentByIdRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        Integer oldId = request.getOldId();
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(request.getNewId());
        studentEntity.setName(request.getName());
        studentEntity.setLastName(request.getLastname());
        studentEntity.setSubject(request.getSubject());
        studentEntity.setCourse(request.getCourse());
        studentService.updateStudentById(studentEntity, oldId);
        TestService.GetStudentsByIdResponse response = TestService.GetStudentsByIdResponse.newBuilder()
                .setId((int) studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();
        responseObserver.onNext(response);
        sendMessageToLog("update student by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void deleteStudentById(TestService.DeleteStudentByIdRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        int id = request.getId();
        StudentEntity studentEntity = studentService.getStudentById(id);
        studentService.deleteStudentById(id);
        TestService.GetStudentsByIdResponse response =
                buildGetStudByIdResponseFromEntity(studentEntity, responseObserver);
        responseObserver.onNext(response);
        sendMessageToLog("delete student by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void getALlStudentsOfTeacher(TestService.GetStudentListOfTeacherRequest request, StreamObserver<TestService.GetStudentListOfTeacherResponse> responseObserver) {
        List<StudentEntity> studentsList = studentService.getAllStudentsOfTeacher(request.getId());
        //will return response iterator
        for (StudentEntity studentEntity : studentsList) {
            TestService.GetStudentListOfTeacherResponse response = TestService.GetStudentListOfTeacherResponse.newBuilder()
                    .setId((int) studentEntity.getId())
                    .setName(studentEntity.getName())
                    .setLastname(studentEntity.getLastName())
                    .setSubject(studentEntity.getSubject())
                    .setCourse(studentEntity.getCourse())
                    .build();
            responseObserver.onNext(response);
            sendMessageToLog("get all students of teacher received" + request + " and sent response " + response);

        }
        responseObserver.onCompleted();
    }

    @Override
    public void attachStudentToTeacher(TestService.AttachStudentToTeacherRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        studentService.attachStudentToTeacher(request.getStudentId(), request.getTeacherId());
        StudentEntity studentEntity = studentService.getStudentById(request.getStudentId());
        TestService.GetStudentsByIdResponse response =
                buildGetStudByIdResponseFromEntity(studentEntity, responseObserver);
        responseObserver.onNext(response);
        sendMessageToLog("delete student by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void deleteStudentFromTeacher(TestService.DeleteStudentFromTeacherRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        studentService.deleteStudentFromTeacher(request.getStudentId(), request.getTeacherId());
        StudentEntity studentEntity = studentService.getStudentById(request.getStudentId());
        TestService.GetStudentsByIdResponse response =
                buildGetStudByIdResponseFromEntity(studentEntity, responseObserver);
        responseObserver.onNext(response);
        sendMessageToLog("delete student from teacher received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }
}
