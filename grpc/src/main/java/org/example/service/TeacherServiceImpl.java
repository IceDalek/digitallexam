package org.example.service;

import com.example.grpc.*;
import io.grpc.stub.StreamObserver;
import org.example.amqp.ActiveMqMessageHelper;
import org.example.mybatis.entity.TeacherEntity;
import org.example.mybatis.service.TeacherService;

import java.util.List;

public class TeacherServiceImpl extends TeacherServiceGrpc.TeacherServiceImplBase {


    private TeacherService teacherService = new TeacherService();

    @Override
    public void getTeacherById(TestService.GetTeacherByIdRequest request,
                               StreamObserver<TestService.GetTeacherByIdResponse> responseObserver) {
        System.out.println(request);

        TeacherEntity teacherEntity = teacherService.getTeacherById(request.getId());
        TestService.GetTeacherByIdResponse response = TestService.GetTeacherByIdResponse.newBuilder()
                .setId((int) teacherEntity.getId())
                .setName(teacherEntity.getName())
                .setLastname(teacherEntity.getLastName())
                .setSubject(teacherEntity.getSubject())
                .build();
        responseObserver.onNext(response);
        System.out.println(response);
        ActiveMqMessageHelper.sendMessageToLog("Get teacher by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();

    }

    @Override
    public void getAllTeachers(TestService.GetAllTeachersRequest request,
                               StreamObserver<TestService.GetTeacherByIdResponse> responseObserver) {

        List<TeacherEntity> teachersList = teacherService.getAllTeachers();
        //will return response iterator
        for (TeacherEntity teacherEntity : teachersList) {
            TestService.GetTeacherByIdResponse response = TestService.GetTeacherByIdResponse.newBuilder()
                    .setId((int) teacherEntity.getId())
                    .setName(teacherEntity.getName())
                    .setLastname(teacherEntity.getLastName())
                    .setSubject(teacherEntity.getSubject())
                    .build();
            responseObserver.onNext(response);
            ActiveMqMessageHelper.sendMessageToLog("Get all teachers received" + request + " and sent response " + response);
            System.out.println("get all teachers send");
            //System.out.println(response);
        }
        responseObserver.onCompleted();
    }

    @Override
    public void createNewTeacher(TestService.AddNewTeacherRequest request, StreamObserver<TestService.GetTeacherByIdResponse> responseObserver) {
        System.out.println("request is " + request);
        System.out.println(request.getId());
        TeacherEntity teacherEntity = new TeacherEntity();
        teacherEntity.setId(request.getId());
        teacherEntity.setName(request.getName());
        teacherEntity.setLastName(request.getLastname());
        teacherEntity.setSubject(request.getSubject());
        teacherService.addNewTeacher(teacherEntity);
        System.out.println("addnewteah");
        TestService.GetTeacherByIdResponse response = TestService.GetTeacherByIdResponse.newBuilder()
                .setId((int) teacherEntity.getId())
                .setName(teacherEntity.getName())
                .setLastname(teacherEntity.getLastName())
                .setSubject(teacherEntity.getSubject())
                .build();
        responseObserver.onNext(response);
        ActiveMqMessageHelper.sendMessageToLog("Get create new teacher received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void updateTeacherById(TestService.UpdateTeacherByIdRequest request, StreamObserver<TestService.GetTeacherByIdResponse> responseObserver) {
        Integer oldId = request.getOldId();
        TeacherEntity teacherEntity = new TeacherEntity();
        teacherEntity.setId(request.getNewId());
        teacherEntity.setName(request.getName());
        teacherEntity.setLastName(request.getLastname());
        teacherEntity.setSubject(request.getSubject());
        teacherService.updateTeacherById(oldId, teacherEntity);
        TestService.GetTeacherByIdResponse response = TestService.GetTeacherByIdResponse.newBuilder()
                .setId((int) teacherEntity.getId())
                .setName(teacherEntity.getName())
                .setLastname(teacherEntity.getLastName())
                .setSubject(teacherEntity.getSubject())
                .build();
        responseObserver.onNext(response);
        ActiveMqMessageHelper.sendMessageToLog("update teacher by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void deleteTeacherById(TestService.DeleteTeacherByIdRequest request, StreamObserver<TestService.GetTeacherByIdResponse> responseObserver) {
        int id = request.getId();
        TeacherEntity teacherEntity = teacherService.getTeacherById(id);
        teacherService.deleteTeacherById(id);
        TestService.GetTeacherByIdResponse response = TestService.GetTeacherByIdResponse.newBuilder()
                .setId((int) teacherEntity.getId())
                .setName(teacherEntity.getName())
                .setLastname(teacherEntity.getLastName())
                .setSubject(teacherEntity.getSubject())
                .build();
        responseObserver.onNext(response);
        ActiveMqMessageHelper.sendMessageToLog("update delete teacher by id received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void getAllTeacherOfStudent(TestService.GetTeacherListOfStudentRequest request, StreamObserver<TestService.GetTeacherListOfStudentResponse> responseObserver) {
        List<TeacherEntity> teachersList = teacherService.getAllTeacherOfStudent(request.getId());
        //will return response iterator
        for (TeacherEntity teacherEntity : teachersList) {
            TestService.GetTeacherListOfStudentResponse response = TestService.GetTeacherListOfStudentResponse.newBuilder()
                    .setId((int) teacherEntity.getId())
                    .setName(teacherEntity.getName())
                    .setLastname(teacherEntity.getLastName())
                    .setSubject(teacherEntity.getSubject())
                    .build();
            responseObserver.onNext(response);
            ActiveMqMessageHelper.sendMessageToLog("Get all teachersOfStudent received" + request + " and sent response " + response);
        }
        responseObserver.onCompleted();
    }
}
