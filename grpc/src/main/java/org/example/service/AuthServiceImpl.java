package org.example.service;

import com.example.grpc.AuthServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.stub.StreamObserver;
import org.example.mybatis.entity.LoginEntity;

import org.example.mybatis.service.AuthService;

import static org.example.amqp.ActiveMqMessageHelper.sendMessageToLog;

public class AuthServiceImpl extends AuthServiceGrpc.AuthServiceImplBase {
    AuthService authService = new AuthService();
    @Override
    public void createAccount(TestService.CreateAccountRequest request, StreamObserver<TestService.CreateAccountResponse> responseObserver) {
        LoginEntity loginEntity = new LoginEntity();
        loginEntity.setUsername(request.getUsername());
        loginEntity.setPassword(request.getPassword());

        TestService.CreateAccountResponse response =
                TestService.CreateAccountResponse.newBuilder().setIsCreated(
                        authService.createAccount(loginEntity))
                        .build();
        responseObserver.onNext(response);
        sendMessageToLog("create account received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }

    @Override
    public void getAccountByUsername(TestService.GetAccountByUsernameRequest request, StreamObserver<TestService.GetAccountByUsernameResponse> responseObserver) {
        LoginEntity loginEntity = authService.getAccountByUsername(request.getUsername());

        TestService.GetAccountByUsernameResponse response =
                TestService.GetAccountByUsernameResponse.newBuilder()
                        .setUsername(loginEntity.getUsername())
                        .setPassword(loginEntity.getPassword())
                        .build();
        responseObserver.onNext(response);
        sendMessageToLog("get account by username received" + request + " and sent response " + response);
        responseObserver.onCompleted();
    }
}
