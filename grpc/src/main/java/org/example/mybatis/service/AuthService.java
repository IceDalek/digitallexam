package org.example.mybatis.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.mybatis.dao.AuthMapper;
import org.example.mybatis.entity.LoginEntity;


import java.io.File;
import java.io.FileReader;
import java.io.Reader;

public class AuthService {
    private final AuthMapper authMapper;

    public AuthService() {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        authMapper = sqlSessionFactory.openSession().getMapper(AuthMapper.class);
    }

    private SqlSessionFactory createSqlSession() {
        SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        String resource = "src/main/resources/mybatis-config.xml";// path of the mybatis configuration file.
        File file = new File(resource);
        System.out.println(file.exists());
        try {
            reader = new FileReader(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlSessionFactory;
    }

    public boolean createAccount(LoginEntity loginEntity) {
        try {
            authMapper.createNewAccount(loginEntity);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public LoginEntity getAccountByUsername(String username) {
        return authMapper.getAccountByUsername(username);
    }
}
