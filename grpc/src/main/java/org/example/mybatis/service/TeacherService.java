package org.example.mybatis.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.amqp.ActiveMqMessageHelper;
import org.example.mybatis.dao.TeacherMapper;
import org.example.mybatis.entity.TeacherEntity;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

public class TeacherService {
    private final TeacherMapper teacherMapper;

    public TeacherService() {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        teacherMapper = sqlSessionFactory.openSession().getMapper(TeacherMapper.class);
    }

    private SqlSessionFactory createSqlSession() {
        SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        String resource = "src/main/resources/mybatis-config.xml";// path of the mybatis configuration file.
        File file = new File(resource);
        try {
            reader = new FileReader(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        } catch (Exception e) {
            e.printStackTrace();
            ActiveMqMessageHelper.sendMessageToLog(this.getClass().getSimpleName() + " " + "failed to open sql session");
        }
        return sqlSessionFactory;
    }

    public TeacherEntity getTeacherById(int id) {
        TeacherEntity teacherEntity;
        teacherEntity = teacherMapper.getTeacherById(id);
        ActiveMqMessageHelper.sendMessageToLog("My batis: getTeacherById returns:+  \n" + teacherEntity);
        return teacherEntity;
    }

    public List<TeacherEntity> getAllTeachers() {
        List<TeacherEntity> teachersList;
        teachersList = teacherMapper.getAllTeachers();
        ActiveMqMessageHelper.sendMessageToLog("My batis: getAllTeacher returns:+  \n" + teachersList);
        return teachersList;
    }

    @Transactional
    public TeacherEntity deleteTeacherById(Integer id) {
        TeacherEntity ret = teacherMapper.getTeacherById(id);
        teacherMapper.deleteTeacherById(id);
        ActiveMqMessageHelper.sendMessageToLog("My batis: deleteTeacherById returns:+  \n" + ret);
        return ret;
    }

    @Transactional
    public TeacherEntity updateTeacherById(Integer id, TeacherEntity teacherEntity) {
        teacherMapper.updateTeacherById(teacherEntity, id);
        TeacherEntity ret = teacherMapper.getTeacherById(id);
        ActiveMqMessageHelper.sendMessageToLog("My batis: updateTeacherById returns:+  \n" + ret);
        return ret;
    }

    @Transactional
    public TeacherEntity addNewTeacher(TeacherEntity teacherEntity) {
        teacherMapper.addNewTeacher(teacherEntity);
        TeacherEntity ret = teacherMapper.getTeacherById(teacherEntity.getId());
        ActiveMqMessageHelper.sendMessageToLog("My batis: addNewTeacher returns:+  \n" + ret);
        return ret;
    }

    public List<TeacherEntity> getAllTeacherOfStudent(Integer studentId) {
        List<TeacherEntity> teacherEntities = teacherMapper.getAllTeacherOfStudent(studentId);
        ActiveMqMessageHelper.sendMessageToLog("My batis: getAllTeacherOfStudent returns:+  \n" + teacherEntities);
        return teacherEntities;
    }

}
