package org.example.mybatis.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.amqp.ActiveMqMessageHelper;
import org.example.mybatis.dao.StudentMapper;
import org.example.mybatis.entity.StudentEntity;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

public class StudentService {
    private final StudentMapper studentMapper;

    public StudentService() {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        sqlSessionFactory.openSession().getMapper(StudentMapper.class);
        this.studentMapper = sqlSessionFactory.openSession().getMapper(StudentMapper.class);

    }

    private SqlSessionFactory createSqlSession() {
        SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        String resource = "src/main/resources/mybatis-config.xml";// path of the mybatis configuration file.
        File file = new File(resource);
        System.out.println(file.exists());
        try {
            reader = new FileReader(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlSessionFactory;
    }

    public StudentEntity getStudentById(int id) {
        StudentEntity studentEntity;
        studentEntity = studentMapper.getStudentById(id);
        ActiveMqMessageHelper.sendMessageToLog("My batis: getStudentById returns:+  \n" + studentEntity);
        return studentEntity;
    }

    public List<StudentEntity> getAllStudents() {
        List<StudentEntity> studentEntities = studentMapper.getAllStudents();
        ActiveMqMessageHelper.sendMessageToLog("My batis: getStudentById returns:+  \n" + studentEntities);
        return studentEntities;
    }

    @Transactional
    public StudentEntity createNewStudent(StudentEntity studentEntity) {
        studentMapper.createNewStudent(studentEntity);
        StudentEntity ret = studentMapper.getStudentById(studentEntity.getId());
        ActiveMqMessageHelper.sendMessageToLog("My batis: createNewStudent returns:+  \n" + ret);
        return ret;
    }

    @Transactional
    public StudentEntity updateStudentById(StudentEntity studentEntity, Integer id) {

        studentMapper.updateStudentById(studentEntity, id);
        StudentEntity ret = studentMapper.getStudentById(studentEntity.getId());
        ActiveMqMessageHelper.sendMessageToLog("My batis: updateStudentById returns:+  \n" + ret);
        return studentEntity;
    }

    @Transactional
    public StudentEntity deleteStudentById(Integer id) {
        StudentEntity ret = studentMapper.getStudentById(id);
        studentMapper.deleteStudentById(id);
        ActiveMqMessageHelper.sendMessageToLog("My batis: deleteStudentById returns:+  \n" + ret);
        return ret;
    }

    public List<StudentEntity> getAllStudentsOfTeacher(Integer id) {
        List<StudentEntity> studentEntities = studentMapper.getStudentsOfTeacher(id);
        ActiveMqMessageHelper.sendMessageToLog("My batis: getAllStudentsOfTeacher returns:+  \n" + studentEntities);
        return studentEntities;
    }

    public StudentEntity attachStudentToTeacher(Integer studentId, Integer teacherId) {
        studentMapper.attachStudentToTeacher(studentId, teacherId);
        StudentEntity ret = studentMapper.getStudentById(studentId);
        ActiveMqMessageHelper.sendMessageToLog("My batis: addStudentToTeacher returns:+  \n" + studentId);
        return ret;
    }

    public StudentEntity deleteStudentFromTeacher(Integer studentId, Integer teacherId) {
        studentMapper.detachStudentFromTeacher(studentId, teacherId);
        return studentMapper.getStudentById(studentId);
    }
}
