package org.example.mybatis.dao;

import org.apache.ibatis.annotations.*;
import org.example.mybatis.entity.TeacherEntity;

import java.util.List;

public interface TeacherMapper {
    @Select("select * from teachers where id = #{id}")
    TeacherEntity getTeacherById(Integer id);

    @Select("select * from teachers")
    List<TeacherEntity> getAllTeachers();

    @Insert("insert into teachers(id, name, last_name, subject) VALUES\n" +
            "(#{id}, #{name}, #{lastName}, #{subject})")
    void addNewTeacher(TeacherEntity teacherEntity);

    @Update("update teachers set id = #{teacherEntity.id}," +
            " name = #{teacherEntity.name}," +
            " last_name = #{teacherEntity.lastName}," +
            " subject = #{teacherEntity.id}\n" +
            "where id = #{id};")
    void updateTeacherById(@Param("teacherEntity") TeacherEntity teacherEntity, @Param("id") Integer id);

    @Delete("delete from teachers where id = #{id}")
    void deleteTeacherById(Integer id);

    @Select("select teacher_id, t.name, t.last_name, t.subject" +
            " from students_teachers" +
            " inner join students s on s.id = students_teachers.student_id " +
            " inner join teachers t on t.id = students_teachers.teacher_id" +
            " where student_id = {#studentId} ")
    List<TeacherEntity> getAllTeacherOfStudent(Integer studentId);


}
