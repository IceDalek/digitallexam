package org.example.mybatis.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.example.mybatis.entity.LoginEntity;

public interface AuthMapper {
@Insert("insert into accounts(username, password) VALUES (#{username}, #{password})")
    void createNewAccount(LoginEntity loginEntity);
@Select("select * from accounts where username = #{username}")
        LoginEntity getAccountByUsername(String username);
}
