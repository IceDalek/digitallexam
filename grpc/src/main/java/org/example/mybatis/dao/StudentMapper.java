package org.example.mybatis.dao;

import org.apache.ibatis.annotations.*;
import org.example.mybatis.entity.StudentEntity;

import java.util.List;

public interface StudentMapper {

    @Select("select * from students where id = #{id}")
    StudentEntity getStudentById(Integer id);

    @Select("Select * from students")
    List<StudentEntity> getAllStudents();

    @Insert("insert into students(id, name, lastName, subject, course) VALUES \n" +
            "(#{id}, #{name}, #{lastName}, #{subject}, #{course})")
    void createNewStudent(StudentEntity studentEntity);

    @Delete("delete from students where id = #{id}")
    void deleteStudentById(Integer id);

    @Update("update students set id = #{studentEntity.id}, name = #{studentEntity.name}," +
            " last_name = #{studentEntity.lastName}, subject = #{studentEntity.subject}," +
            " course = #{studentEntity.course} where id = #{id}")
    void updateStudentById(@Param("StudentEntity") StudentEntity studentEntity, @Param("id") Integer id);



    @Select("select s.id, s.name, s.last_name, s.subject, s.course from students_teachers inner join students s on s.id = students_teachers.student_id\n" +
            "inner join teachers t on t.id = students_teachers.teacher_id where teacher_id = #{id};")
    List<StudentEntity> getStudentsOfTeacher(Integer id);

    @Insert("insert into students_teachers (student_id, teacher_id) VALUES (#{sid}, #{tid})")
    void attachStudentToTeacher(@Param("sid") Integer studentId, @Param("tid") Integer teacherId);

    @Delete("delete from students_teachers where student_id = #{sid} and teacher_id = #{tid}")
    void detachStudentFromTeacher(@Param("sid") Integer studentId, @Param("tid") Integer teacherId);
}
