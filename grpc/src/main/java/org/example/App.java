package org.example;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import lombok.extern.slf4j.Slf4j;
import org.example.service.AuthServiceImpl;
import org.example.service.StudentServiceImpl;
import org.example.service.TeacherServiceImpl;

import java.io.IOException;


@Slf4j
public class App {

    public static void main(String[] args) throws IOException, InterruptedException {

        Server server = ServerBuilder.forPort(8020).
                addService(new TeacherServiceImpl())
                .addService(new StudentServiceImpl())
                .addService(new AuthServiceImpl())
                .build();
        server.start();
        System.out.println("started server");
        server.awaitTermination();
    }
}
