package org.example.amqp;

public class ActiveMqMessageHelper {
    public static void sendMessageToLog(String message) {
        Thread brokerThread = new Thread(new InfoPublisher(message));
        brokerThread.setDaemon(false);
        brokerThread.start();
    }
}
