package com.example.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.24.0)",
    comments = "Source: TestService.proto")
public final class TeacherServiceGrpc {

  private TeacherServiceGrpc() {}

  public static final String SERVICE_NAME = "com.example.grpc.TeacherService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.GetTeacherByIdRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getGetTeacherByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getTeacherById",
      requestType = com.example.grpc.TestService.GetTeacherByIdRequest.class,
      responseType = com.example.grpc.TestService.GetTeacherByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.GetTeacherByIdRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getGetTeacherByIdMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.GetTeacherByIdRequest, com.example.grpc.TestService.GetTeacherByIdResponse> getGetTeacherByIdMethod;
    if ((getGetTeacherByIdMethod = TeacherServiceGrpc.getGetTeacherByIdMethod) == null) {
      synchronized (TeacherServiceGrpc.class) {
        if ((getGetTeacherByIdMethod = TeacherServiceGrpc.getGetTeacherByIdMethod) == null) {
          TeacherServiceGrpc.getGetTeacherByIdMethod = getGetTeacherByIdMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.GetTeacherByIdRequest, com.example.grpc.TestService.GetTeacherByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getTeacherById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TeacherServiceMethodDescriptorSupplier("getTeacherById"))
              .build();
        }
      }
    }
    return getGetTeacherByIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.GetAllTeachersRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getGetAllTeachersMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllTeachers",
      requestType = com.example.grpc.TestService.GetAllTeachersRequest.class,
      responseType = com.example.grpc.TestService.GetTeacherByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.GetAllTeachersRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getGetAllTeachersMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.GetAllTeachersRequest, com.example.grpc.TestService.GetTeacherByIdResponse> getGetAllTeachersMethod;
    if ((getGetAllTeachersMethod = TeacherServiceGrpc.getGetAllTeachersMethod) == null) {
      synchronized (TeacherServiceGrpc.class) {
        if ((getGetAllTeachersMethod = TeacherServiceGrpc.getGetAllTeachersMethod) == null) {
          TeacherServiceGrpc.getGetAllTeachersMethod = getGetAllTeachersMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.GetAllTeachersRequest, com.example.grpc.TestService.GetTeacherByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getAllTeachers"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetAllTeachersRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TeacherServiceMethodDescriptorSupplier("getAllTeachers"))
              .build();
        }
      }
    }
    return getGetAllTeachersMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.AddNewTeacherRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getCreateNewTeacherMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "createNewTeacher",
      requestType = com.example.grpc.TestService.AddNewTeacherRequest.class,
      responseType = com.example.grpc.TestService.GetTeacherByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.AddNewTeacherRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getCreateNewTeacherMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.AddNewTeacherRequest, com.example.grpc.TestService.GetTeacherByIdResponse> getCreateNewTeacherMethod;
    if ((getCreateNewTeacherMethod = TeacherServiceGrpc.getCreateNewTeacherMethod) == null) {
      synchronized (TeacherServiceGrpc.class) {
        if ((getCreateNewTeacherMethod = TeacherServiceGrpc.getCreateNewTeacherMethod) == null) {
          TeacherServiceGrpc.getCreateNewTeacherMethod = getCreateNewTeacherMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.AddNewTeacherRequest, com.example.grpc.TestService.GetTeacherByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "createNewTeacher"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.AddNewTeacherRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TeacherServiceMethodDescriptorSupplier("createNewTeacher"))
              .build();
        }
      }
    }
    return getCreateNewTeacherMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.UpdateTeacherByIdRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getUpdateTeacherByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateTeacherById",
      requestType = com.example.grpc.TestService.UpdateTeacherByIdRequest.class,
      responseType = com.example.grpc.TestService.GetTeacherByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.UpdateTeacherByIdRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getUpdateTeacherByIdMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.UpdateTeacherByIdRequest, com.example.grpc.TestService.GetTeacherByIdResponse> getUpdateTeacherByIdMethod;
    if ((getUpdateTeacherByIdMethod = TeacherServiceGrpc.getUpdateTeacherByIdMethod) == null) {
      synchronized (TeacherServiceGrpc.class) {
        if ((getUpdateTeacherByIdMethod = TeacherServiceGrpc.getUpdateTeacherByIdMethod) == null) {
          TeacherServiceGrpc.getUpdateTeacherByIdMethod = getUpdateTeacherByIdMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.UpdateTeacherByIdRequest, com.example.grpc.TestService.GetTeacherByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateTeacherById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.UpdateTeacherByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TeacherServiceMethodDescriptorSupplier("updateTeacherById"))
              .build();
        }
      }
    }
    return getUpdateTeacherByIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteTeacherByIdRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getDeleteTeacherByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deleteTeacherById",
      requestType = com.example.grpc.TestService.DeleteTeacherByIdRequest.class,
      responseType = com.example.grpc.TestService.GetTeacherByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteTeacherByIdRequest,
      com.example.grpc.TestService.GetTeacherByIdResponse> getDeleteTeacherByIdMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteTeacherByIdRequest, com.example.grpc.TestService.GetTeacherByIdResponse> getDeleteTeacherByIdMethod;
    if ((getDeleteTeacherByIdMethod = TeacherServiceGrpc.getDeleteTeacherByIdMethod) == null) {
      synchronized (TeacherServiceGrpc.class) {
        if ((getDeleteTeacherByIdMethod = TeacherServiceGrpc.getDeleteTeacherByIdMethod) == null) {
          TeacherServiceGrpc.getDeleteTeacherByIdMethod = getDeleteTeacherByIdMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.DeleteTeacherByIdRequest, com.example.grpc.TestService.GetTeacherByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "deleteTeacherById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.DeleteTeacherByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TeacherServiceMethodDescriptorSupplier("deleteTeacherById"))
              .build();
        }
      }
    }
    return getDeleteTeacherByIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.GetTeacherListOfStudentRequest,
      com.example.grpc.TestService.GetTeacherListOfStudentResponse> getGetAllTeacherOfStudentMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllTeacherOfStudent",
      requestType = com.example.grpc.TestService.GetTeacherListOfStudentRequest.class,
      responseType = com.example.grpc.TestService.GetTeacherListOfStudentResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.GetTeacherListOfStudentRequest,
      com.example.grpc.TestService.GetTeacherListOfStudentResponse> getGetAllTeacherOfStudentMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.GetTeacherListOfStudentRequest, com.example.grpc.TestService.GetTeacherListOfStudentResponse> getGetAllTeacherOfStudentMethod;
    if ((getGetAllTeacherOfStudentMethod = TeacherServiceGrpc.getGetAllTeacherOfStudentMethod) == null) {
      synchronized (TeacherServiceGrpc.class) {
        if ((getGetAllTeacherOfStudentMethod = TeacherServiceGrpc.getGetAllTeacherOfStudentMethod) == null) {
          TeacherServiceGrpc.getGetAllTeacherOfStudentMethod = getGetAllTeacherOfStudentMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.GetTeacherListOfStudentRequest, com.example.grpc.TestService.GetTeacherListOfStudentResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getAllTeacherOfStudent"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherListOfStudentRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetTeacherListOfStudentResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TeacherServiceMethodDescriptorSupplier("getAllTeacherOfStudent"))
              .build();
        }
      }
    }
    return getGetAllTeacherOfStudentMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static TeacherServiceStub newStub(io.grpc.Channel channel) {
    return new TeacherServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static TeacherServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new TeacherServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static TeacherServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new TeacherServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class TeacherServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getTeacherById(com.example.grpc.TestService.GetTeacherByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetTeacherByIdMethod(), responseObserver);
    }

    /**
     */
    public void getAllTeachers(com.example.grpc.TestService.GetAllTeachersRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllTeachersMethod(), responseObserver);
    }

    /**
     */
    public void createNewTeacher(com.example.grpc.TestService.AddNewTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateNewTeacherMethod(), responseObserver);
    }

    /**
     */
    public void updateTeacherById(com.example.grpc.TestService.UpdateTeacherByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateTeacherByIdMethod(), responseObserver);
    }

    /**
     */
    public void deleteTeacherById(com.example.grpc.TestService.DeleteTeacherByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteTeacherByIdMethod(), responseObserver);
    }

    /**
     */
    public void getAllTeacherOfStudent(com.example.grpc.TestService.GetTeacherListOfStudentRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherListOfStudentResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllTeacherOfStudentMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetTeacherByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.GetTeacherByIdRequest,
                com.example.grpc.TestService.GetTeacherByIdResponse>(
                  this, METHODID_GET_TEACHER_BY_ID)))
          .addMethod(
            getGetAllTeachersMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.example.grpc.TestService.GetAllTeachersRequest,
                com.example.grpc.TestService.GetTeacherByIdResponse>(
                  this, METHODID_GET_ALL_TEACHERS)))
          .addMethod(
            getCreateNewTeacherMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.AddNewTeacherRequest,
                com.example.grpc.TestService.GetTeacherByIdResponse>(
                  this, METHODID_CREATE_NEW_TEACHER)))
          .addMethod(
            getUpdateTeacherByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.UpdateTeacherByIdRequest,
                com.example.grpc.TestService.GetTeacherByIdResponse>(
                  this, METHODID_UPDATE_TEACHER_BY_ID)))
          .addMethod(
            getDeleteTeacherByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.DeleteTeacherByIdRequest,
                com.example.grpc.TestService.GetTeacherByIdResponse>(
                  this, METHODID_DELETE_TEACHER_BY_ID)))
          .addMethod(
            getGetAllTeacherOfStudentMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.example.grpc.TestService.GetTeacherListOfStudentRequest,
                com.example.grpc.TestService.GetTeacherListOfStudentResponse>(
                  this, METHODID_GET_ALL_TEACHER_OF_STUDENT)))
          .build();
    }
  }

  /**
   */
  public static final class TeacherServiceStub extends io.grpc.stub.AbstractStub<TeacherServiceStub> {
    private TeacherServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TeacherServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TeacherServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TeacherServiceStub(channel, callOptions);
    }

    /**
     */
    public void getTeacherById(com.example.grpc.TestService.GetTeacherByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetTeacherByIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getAllTeachers(com.example.grpc.TestService.GetAllTeachersRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllTeachersMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createNewTeacher(com.example.grpc.TestService.AddNewTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateNewTeacherMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateTeacherById(com.example.grpc.TestService.UpdateTeacherByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateTeacherByIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteTeacherById(com.example.grpc.TestService.DeleteTeacherByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteTeacherByIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getAllTeacherOfStudent(com.example.grpc.TestService.GetTeacherListOfStudentRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherListOfStudentResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllTeacherOfStudentMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class TeacherServiceBlockingStub extends io.grpc.stub.AbstractStub<TeacherServiceBlockingStub> {
    private TeacherServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TeacherServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TeacherServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TeacherServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.example.grpc.TestService.GetTeacherByIdResponse getTeacherById(com.example.grpc.TestService.GetTeacherByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetTeacherByIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<com.example.grpc.TestService.GetTeacherByIdResponse> getAllTeachers(
        com.example.grpc.TestService.GetAllTeachersRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllTeachersMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetTeacherByIdResponse createNewTeacher(com.example.grpc.TestService.AddNewTeacherRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateNewTeacherMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetTeacherByIdResponse updateTeacherById(com.example.grpc.TestService.UpdateTeacherByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateTeacherByIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetTeacherByIdResponse deleteTeacherById(com.example.grpc.TestService.DeleteTeacherByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getDeleteTeacherByIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<com.example.grpc.TestService.GetTeacherListOfStudentResponse> getAllTeacherOfStudent(
        com.example.grpc.TestService.GetTeacherListOfStudentRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllTeacherOfStudentMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class TeacherServiceFutureStub extends io.grpc.stub.AbstractStub<TeacherServiceFutureStub> {
    private TeacherServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TeacherServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TeacherServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TeacherServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetTeacherByIdResponse> getTeacherById(
        com.example.grpc.TestService.GetTeacherByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetTeacherByIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetTeacherByIdResponse> createNewTeacher(
        com.example.grpc.TestService.AddNewTeacherRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateNewTeacherMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetTeacherByIdResponse> updateTeacherById(
        com.example.grpc.TestService.UpdateTeacherByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateTeacherByIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetTeacherByIdResponse> deleteTeacherById(
        com.example.grpc.TestService.DeleteTeacherByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteTeacherByIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_TEACHER_BY_ID = 0;
  private static final int METHODID_GET_ALL_TEACHERS = 1;
  private static final int METHODID_CREATE_NEW_TEACHER = 2;
  private static final int METHODID_UPDATE_TEACHER_BY_ID = 3;
  private static final int METHODID_DELETE_TEACHER_BY_ID = 4;
  private static final int METHODID_GET_ALL_TEACHER_OF_STUDENT = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final TeacherServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(TeacherServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_TEACHER_BY_ID:
          serviceImpl.getTeacherById((com.example.grpc.TestService.GetTeacherByIdRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse>) responseObserver);
          break;
        case METHODID_GET_ALL_TEACHERS:
          serviceImpl.getAllTeachers((com.example.grpc.TestService.GetAllTeachersRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse>) responseObserver);
          break;
        case METHODID_CREATE_NEW_TEACHER:
          serviceImpl.createNewTeacher((com.example.grpc.TestService.AddNewTeacherRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse>) responseObserver);
          break;
        case METHODID_UPDATE_TEACHER_BY_ID:
          serviceImpl.updateTeacherById((com.example.grpc.TestService.UpdateTeacherByIdRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse>) responseObserver);
          break;
        case METHODID_DELETE_TEACHER_BY_ID:
          serviceImpl.deleteTeacherById((com.example.grpc.TestService.DeleteTeacherByIdRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherByIdResponse>) responseObserver);
          break;
        case METHODID_GET_ALL_TEACHER_OF_STUDENT:
          serviceImpl.getAllTeacherOfStudent((com.example.grpc.TestService.GetTeacherListOfStudentRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetTeacherListOfStudentResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class TeacherServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    TeacherServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.grpc.TestService.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("TeacherService");
    }
  }

  private static final class TeacherServiceFileDescriptorSupplier
      extends TeacherServiceBaseDescriptorSupplier {
    TeacherServiceFileDescriptorSupplier() {}
  }

  private static final class TeacherServiceMethodDescriptorSupplier
      extends TeacherServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    TeacherServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (TeacherServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new TeacherServiceFileDescriptorSupplier())
              .addMethod(getGetTeacherByIdMethod())
              .addMethod(getGetAllTeachersMethod())
              .addMethod(getCreateNewTeacherMethod())
              .addMethod(getUpdateTeacherByIdMethod())
              .addMethod(getDeleteTeacherByIdMethod())
              .addMethod(getGetAllTeacherOfStudentMethod())
              .build();
        }
      }
    }
    return result;
  }
}
