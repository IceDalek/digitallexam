package org.example.utils;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;


import org.example.entitiy.LoginEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PrincipalUtils {

    public static User getUserDetailsFromLoginEntity(LoginEntity loginEntity, String login, List<GrantedAuthority> authorities) {
        return new User(loginEntity.getUsername(), loginEntity.getPassword(), authorities);
    }
}
