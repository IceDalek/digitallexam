package org.example.api;

import org.example.entitiy.StudentEntity;

import java.util.List;

public interface StudentService {
    StudentEntity getStudentById(Integer id);

    List<StudentEntity> getAllStudents();

    StudentEntity createNewStudent(StudentEntity studentEntity);

    StudentEntity deleteStudentById(Integer id);

    StudentEntity updateStudentById(StudentEntity studentEntity, Integer id);

    List<StudentEntity> getStudentsOfTeacher(Integer id);

    StudentEntity attachStudentToTeacher(Integer studentId, Integer teacherId);

    StudentEntity detachStudentFromTeacher(Integer studentId, Integer teacherId);
}
