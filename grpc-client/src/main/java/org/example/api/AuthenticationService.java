package org.example.api;

import org.example.entitiy.LoginEntity;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;

public interface AuthenticationService {
    @Transactional
    Authentication authorize(LoginEntity loginEntity);

    void register(LoginEntity loginEntity);

    void logout();
}
