package org.example.api;

import org.example.entitiy.TeacherEntity;

import java.util.List;

public interface TeacherService {

    TeacherEntity getTeacherById(Integer teacherId);

    List<TeacherEntity> getAllTeachers();

    TeacherEntity addNewTeacher(TeacherEntity teacherEntity);

    TeacherEntity deleteTeacherById(Integer teacherId);

    TeacherEntity updateTeacherById(Integer teacher_id, TeacherEntity teacherEntity);
}
