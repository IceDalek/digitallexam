package org.example;

import com.example.grpc.StudentsServiceGrpc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Slf4j
@SpringBootApplication
public class UnivercityClient {
    public static void main(String[] args) {
        SpringApplication.run(UnivercityClient.class);
    }
}
