package org.example.serivce;

import com.example.grpc.StudentsServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.example.amqp.ActiveMqMessageHelper;
import org.example.api.StudentService;
import org.example.entitiy.StudentEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private StudentEntity getStudentEntityFromResponse(TestService.GetStudentsByIdResponse response) {
        StudentEntity ret = new StudentEntity();
        ret.setId(response.getId());
        ret.setName(response.getName());
        ret.setLastName(response.getLastname());
        ret.setSubject(response.getSubject());
        ret.setCourse(response.getCourse());
        return ret;
    }

    @Override
    public StudentEntity getStudentById(Integer id) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.GetStudentByIdRequest request
                = TestService.GetStudentByIdRequest.newBuilder().setId(id).build();

        TestService.GetStudentsByIdResponse response = stub.getStudentById(request);
        System.out.println(response);
        channel.shutdownNow();

        return null;
    }


    public List<StudentEntity> getAllStudents() {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.GetAllStudentsRequest request
                = TestService.GetAllStudentsRequest.newBuilder().setStub("").build();
        Iterator students = stub.getAllStudents(request);
        List<StudentEntity> ret = new ArrayList<>();
        while (students.hasNext()) {
            ret.add((StudentEntity) students.next());
        }
        ActiveMqMessageHelper.sendMessageToLog("Client(getAllStudents) send request:  " + request + " \n " +
                "and received response(to list): " + ret);
        return ret;
    }

    @Override
    public StudentEntity createNewStudent(StudentEntity studentEntity) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.AddNewStudentRequest request
                = TestService.AddNewStudentRequest.newBuilder()
                .setId(studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();

        TestService.GetStudentsByIdResponse response = stub.createNewStudent(request);
        ActiveMqMessageHelper.sendMessageToLog("Client(createNewStudent) send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();
        return getStudentEntityFromResponse(response);

    }

    @Override
    public StudentEntity updateStudentById(StudentEntity studentEntity, Integer id) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.UpdateStudentByIdRequest request
                = TestService.UpdateStudentByIdRequest.newBuilder().
                setOldId(id).
                setNewId(studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();

        TestService.GetStudentsByIdResponse response = stub.updateStudentById(request);
        System.out.println("server send update response + " + response);
        StudentEntity ret = getStudentEntityFromResponse(response);
        ActiveMqMessageHelper.sendMessageToLog("Client(updateStudentById) send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();
        return ret;

    }

    @Override
    public StudentEntity deleteStudentById(Integer id) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.DeleteStudentByIdRequest request
                = TestService.DeleteStudentByIdRequest.newBuilder()
                .setId(id)
                .build();
        TestService.GetStudentsByIdResponse response = stub.deleteStudentById(request);
        ActiveMqMessageHelper.sendMessageToLog("Client(deleteStudentById) send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();
        return getStudentEntityFromResponse(response);

    }


    public List<StudentEntity> getStudentsOfTeacher(Integer id) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.GetStudentListOfTeacherRequest request
                = TestService.GetStudentListOfTeacherRequest.newBuilder().setId(id).build();
        Iterator students = stub.getALlStudentsOfTeacher(request);
        List<StudentEntity> ret = new ArrayList<>();
        while (students.hasNext()) {
            ret.add((StudentEntity) students.next());
        }
        ActiveMqMessageHelper.sendMessageToLog("Client(getStudentsOfTeacher) send request:  " + request + " \n " +
                "and received response(to list): " + ret);
        return ret;
    }

    @Override
    public StudentEntity attachStudentToTeacher(Integer studentId, Integer teacherId) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.AttachStudentToTeacherRequest request
                = TestService.AttachStudentToTeacherRequest.newBuilder()
                .setStudentId(studentId)
                .setTeacherId(teacherId)
                .build();
        TestService.GetStudentsByIdResponse response = stub.attachStudentToTeacher(request);
        ActiveMqMessageHelper.sendMessageToLog("Client(attachStudentToTeacher) send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();
        return getStudentEntityFromResponse(response);
    }


    public StudentEntity detachStudentFromTeacher(Integer studentId, Integer teacherId) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.DeleteStudentFromTeacherRequest request
                = TestService.DeleteStudentFromTeacherRequest.newBuilder()
                .setStudentId(studentId)
                .setTeacherId(teacherId)
                .build();
        TestService.GetStudentsByIdResponse response = stub.deleteStudentFromTeacher(request);
        ActiveMqMessageHelper.sendMessageToLog("Client(detachStudentFromTeacher) send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();
        return getStudentEntityFromResponse(response);
    }

}
