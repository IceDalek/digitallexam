package org.example.serivce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import org.example.entitiy.LoginEntity;
import org.example.utils.PrincipalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AuthServiceImpl authService = new AuthServiceImpl();

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        LoginEntity loginEntity = authService.getAccountByUsername(login);
        return PrincipalUtils.getUserDetailsFromLoginEntity(loginEntity, login, getAuthorities());
    }

    private List<GrantedAuthority> getAuthorities() {
        return Collections.singletonList((GrantedAuthority) new SimpleGrantedAuthority("ROLE_EXAMPLE"));
    }


}

