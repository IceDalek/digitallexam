package org.example.serivce;

;

import com.example.grpc.AuthServiceGrpc;
import com.example.grpc.StudentsServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.example.amqp.ActiveMqMessageHelper;
import org.example.api.AuthenticationService;
import org.example.entitiy.LoginEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

@Service
public class AuthServiceImpl implements AuthenticationService {


    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    @Transactional
    public Authentication authorize(LoginEntity LoginEntity) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(LoginEntity.getUsername(), LoginEntity.getPassword());
        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication;
    }

    @Override
    public void register(LoginEntity loginEntity) {
        System.out.println("test");
        System.out.println(loginEntity);
        loginEntity.setPassword((loginEntity.getPassword()));
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        AuthServiceGrpc.AuthServiceBlockingStub stub =
                AuthServiceGrpc.newBlockingStub(channel);
        TestService.CreateAccountRequest request =
                TestService.CreateAccountRequest.newBuilder()
                        .setUsername(loginEntity.getUsername())
                .setPassword(loginEntity.getPassword()).build();
        TestService.CreateAccountResponse response = stub.createAccount(request);
        ActiveMqMessageHelper.sendMessageToLog("Client(createNewAccount) send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();

    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    public LoginEntity getAccountByUsername(String username){
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        AuthServiceGrpc.AuthServiceBlockingStub stub =
                AuthServiceGrpc.newBlockingStub(channel);
        TestService.GetAccountByUsernameRequest request =
                TestService.GetAccountByUsernameRequest.newBuilder()
                        .setUsername(username)
                        .build();
        TestService.GetAccountByUsernameResponse response = stub.getAccountByUsername(request);
        ActiveMqMessageHelper.sendMessageToLog("getAccountByUsername send request:  " + request + " \n " +
                "and received response: " + response);
        channel.shutdownNow();
        LoginEntity loginEntity = new LoginEntity();
        loginEntity.setUsername(response.getUsername());
        loginEntity.setPassword(response.getPassword());
        return loginEntity;
    }

}

