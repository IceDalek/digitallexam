package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.entitiy.TeacherEntity;
import org.example.serivce.TeacherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/teachers")
public class TeachersController {
    @Autowired
    TeacherServiceImpl teacherService;

    @GetMapping("{teacherId}")
    public TeacherEntity getTeacherById(@PathVariable Integer teacherId) {
        return teacherService.getTeacherById(teacherId);
    }

    @GetMapping()
    public List<TeacherEntity> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @PostMapping("/new")
    public TeacherEntity addNewTeacher(@RequestBody TeacherEntity teacherEntity) {
        return teacherService.addNewTeacher(teacherEntity);
    }

    @DeleteMapping("{teacherId}")
    public TeacherEntity deleteTeacherById(@PathVariable Integer teacherId) {
        return teacherService.deleteTeacherById(teacherId);
    }

    @PostMapping("update/{teacher_id}")
    public TeacherEntity updateTeacherById(@PathVariable Integer teacher_id,
                                           @RequestBody TeacherEntity teacherEntity) {
        return teacherService.updateTeacherById(teacher_id, teacherEntity);
    }
}
