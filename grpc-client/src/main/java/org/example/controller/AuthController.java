package org.example.controller;

import org.example.entitiy.LoginEntity;
import org.example.serivce.AuthServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    private AuthServiceImpl authService = new AuthServiceImpl();

    @PostMapping("/register")
    void register(@RequestBody LoginEntity loginEntity) {
        authService.register(loginEntity);
    }

    @PostMapping("/auth")
    void auth(@RequestBody LoginEntity loginEntity) {
        authService.authorize(loginEntity);
    }

    @GetMapping("/logout")
    void logout() {
        authService.logout();
    }

}
