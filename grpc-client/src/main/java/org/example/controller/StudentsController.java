package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.entitiy.StudentEntity;
import org.example.serivce.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/students")
public class StudentsController {
    @Autowired
    StudentServiceImpl studentService;

    @GetMapping("{studentId}")
    public StudentEntity getStudentById(@PathVariable Integer studentId) {
        return studentService.getStudentById(studentId);
    }

    @GetMapping()
    public List<StudentEntity> GetAllStudents() {
        return studentService.getAllStudents();
    }

    @PostMapping("/new")
    public StudentEntity addNewStudent(@RequestBody StudentEntity studentEntity) {
        return studentService.createNewStudent(studentEntity);
    }

    @DeleteMapping("{studentId}")
    public StudentEntity deleteStudentById(@PathVariable Integer studentId) {
        return studentService.deleteStudentById(studentId);
    }

    @PostMapping("update/{student_id}")
    public StudentEntity updateStudentById(@PathVariable Integer student_id,
                                           @RequestBody StudentEntity studentEntity) {
        return studentService.updateStudentById(studentEntity, student_id);
    }

    @GetMapping("teachers/{teacher_id}")
    public List<StudentEntity> getStudentsOfTeacher(@PathVariable Integer teacher_id) {
        return studentService.getStudentsOfTeacher(teacher_id);
    }

    @PostMapping("/attach/student/{student_id}/teacher/{teacher_id}")
    public StudentEntity attachStudentToTeacher(@PathVariable Integer student_id,
                                                @PathVariable Integer teacher_id) {
        return studentService.attachStudentToTeacher(student_id, teacher_id);
    }

    @PostMapping("/detach/student/{student_id}/teacher/{teacher_id}")
    public StudentEntity detachStudentToTeacher(@PathVariable Integer student_id,
                                                @PathVariable Integer teacher_id) {
        return studentService.detachStudentFromTeacher(student_id, teacher_id);
    }


}
