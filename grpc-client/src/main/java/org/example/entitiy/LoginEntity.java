package org.example.entitiy;

import lombok.Data;

@Data
public class LoginEntity {
    private String username;
    private String password;
}
