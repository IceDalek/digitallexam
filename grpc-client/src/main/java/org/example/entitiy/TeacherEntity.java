package org.example.entitiy;


import lombok.Data;

@Data
public class TeacherEntity {
    private Integer id;
    private String name;
    private String lastName;
    private String subject;
}
