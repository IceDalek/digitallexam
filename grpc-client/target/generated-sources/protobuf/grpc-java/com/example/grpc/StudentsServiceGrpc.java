package com.example.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.24.0)",
    comments = "Source: TestService.proto")
public final class StudentsServiceGrpc {

  private StudentsServiceGrpc() {}

  public static final String SERVICE_NAME = "com.example.grpc.StudentsService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.GetStudentByIdRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getGetStudentByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getStudentById",
      requestType = com.example.grpc.TestService.GetStudentByIdRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.GetStudentByIdRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getGetStudentByIdMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.GetStudentByIdRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getGetStudentByIdMethod;
    if ((getGetStudentByIdMethod = StudentsServiceGrpc.getGetStudentByIdMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getGetStudentByIdMethod = StudentsServiceGrpc.getGetStudentByIdMethod) == null) {
          StudentsServiceGrpc.getGetStudentByIdMethod = getGetStudentByIdMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.GetStudentByIdRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getStudentById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("getStudentById"))
              .build();
        }
      }
    }
    return getGetStudentByIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.GetAllStudentsRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getGetAllStudentsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllStudents",
      requestType = com.example.grpc.TestService.GetAllStudentsRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.GetAllStudentsRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getGetAllStudentsMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.GetAllStudentsRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getGetAllStudentsMethod;
    if ((getGetAllStudentsMethod = StudentsServiceGrpc.getGetAllStudentsMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getGetAllStudentsMethod = StudentsServiceGrpc.getGetAllStudentsMethod) == null) {
          StudentsServiceGrpc.getGetAllStudentsMethod = getGetAllStudentsMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.GetAllStudentsRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getAllStudents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetAllStudentsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("getAllStudents"))
              .build();
        }
      }
    }
    return getGetAllStudentsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.AddNewStudentRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getCreateNewStudentMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "createNewStudent",
      requestType = com.example.grpc.TestService.AddNewStudentRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.AddNewStudentRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getCreateNewStudentMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.AddNewStudentRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getCreateNewStudentMethod;
    if ((getCreateNewStudentMethod = StudentsServiceGrpc.getCreateNewStudentMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getCreateNewStudentMethod = StudentsServiceGrpc.getCreateNewStudentMethod) == null) {
          StudentsServiceGrpc.getCreateNewStudentMethod = getCreateNewStudentMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.AddNewStudentRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "createNewStudent"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.AddNewStudentRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("createNewStudent"))
              .build();
        }
      }
    }
    return getCreateNewStudentMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.UpdateStudentByIdRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getUpdateStudentByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateStudentById",
      requestType = com.example.grpc.TestService.UpdateStudentByIdRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.UpdateStudentByIdRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getUpdateStudentByIdMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.UpdateStudentByIdRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getUpdateStudentByIdMethod;
    if ((getUpdateStudentByIdMethod = StudentsServiceGrpc.getUpdateStudentByIdMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getUpdateStudentByIdMethod = StudentsServiceGrpc.getUpdateStudentByIdMethod) == null) {
          StudentsServiceGrpc.getUpdateStudentByIdMethod = getUpdateStudentByIdMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.UpdateStudentByIdRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateStudentById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.UpdateStudentByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("updateStudentById"))
              .build();
        }
      }
    }
    return getUpdateStudentByIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteStudentByIdRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getDeleteStudentByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deleteStudentById",
      requestType = com.example.grpc.TestService.DeleteStudentByIdRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteStudentByIdRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getDeleteStudentByIdMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteStudentByIdRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getDeleteStudentByIdMethod;
    if ((getDeleteStudentByIdMethod = StudentsServiceGrpc.getDeleteStudentByIdMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getDeleteStudentByIdMethod = StudentsServiceGrpc.getDeleteStudentByIdMethod) == null) {
          StudentsServiceGrpc.getDeleteStudentByIdMethod = getDeleteStudentByIdMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.DeleteStudentByIdRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "deleteStudentById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.DeleteStudentByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("deleteStudentById"))
              .build();
        }
      }
    }
    return getDeleteStudentByIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.GetStudentListOfTeacherRequest,
      com.example.grpc.TestService.GetStudentListOfTeacherResponse> getGetALlStudentsOfTeacherMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getALlStudentsOfTeacher",
      requestType = com.example.grpc.TestService.GetStudentListOfTeacherRequest.class,
      responseType = com.example.grpc.TestService.GetStudentListOfTeacherResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.GetStudentListOfTeacherRequest,
      com.example.grpc.TestService.GetStudentListOfTeacherResponse> getGetALlStudentsOfTeacherMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.GetStudentListOfTeacherRequest, com.example.grpc.TestService.GetStudentListOfTeacherResponse> getGetALlStudentsOfTeacherMethod;
    if ((getGetALlStudentsOfTeacherMethod = StudentsServiceGrpc.getGetALlStudentsOfTeacherMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getGetALlStudentsOfTeacherMethod = StudentsServiceGrpc.getGetALlStudentsOfTeacherMethod) == null) {
          StudentsServiceGrpc.getGetALlStudentsOfTeacherMethod = getGetALlStudentsOfTeacherMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.GetStudentListOfTeacherRequest, com.example.grpc.TestService.GetStudentListOfTeacherResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getALlStudentsOfTeacher"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentListOfTeacherRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentListOfTeacherResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("getALlStudentsOfTeacher"))
              .build();
        }
      }
    }
    return getGetALlStudentsOfTeacherMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.AttachStudentToTeacherRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getAttachStudentToTeacherMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "attachStudentToTeacher",
      requestType = com.example.grpc.TestService.AttachStudentToTeacherRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.AttachStudentToTeacherRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getAttachStudentToTeacherMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.AttachStudentToTeacherRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getAttachStudentToTeacherMethod;
    if ((getAttachStudentToTeacherMethod = StudentsServiceGrpc.getAttachStudentToTeacherMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getAttachStudentToTeacherMethod = StudentsServiceGrpc.getAttachStudentToTeacherMethod) == null) {
          StudentsServiceGrpc.getAttachStudentToTeacherMethod = getAttachStudentToTeacherMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.AttachStudentToTeacherRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "attachStudentToTeacher"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.AttachStudentToTeacherRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("attachStudentToTeacher"))
              .build();
        }
      }
    }
    return getAttachStudentToTeacherMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteStudentFromTeacherRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getDeleteStudentFromTeacherMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deleteStudentFromTeacher",
      requestType = com.example.grpc.TestService.DeleteStudentFromTeacherRequest.class,
      responseType = com.example.grpc.TestService.GetStudentsByIdResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteStudentFromTeacherRequest,
      com.example.grpc.TestService.GetStudentsByIdResponse> getDeleteStudentFromTeacherMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.TestService.DeleteStudentFromTeacherRequest, com.example.grpc.TestService.GetStudentsByIdResponse> getDeleteStudentFromTeacherMethod;
    if ((getDeleteStudentFromTeacherMethod = StudentsServiceGrpc.getDeleteStudentFromTeacherMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getDeleteStudentFromTeacherMethod = StudentsServiceGrpc.getDeleteStudentFromTeacherMethod) == null) {
          StudentsServiceGrpc.getDeleteStudentFromTeacherMethod = getDeleteStudentFromTeacherMethod =
              io.grpc.MethodDescriptor.<com.example.grpc.TestService.DeleteStudentFromTeacherRequest, com.example.grpc.TestService.GetStudentsByIdResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "deleteStudentFromTeacher"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.DeleteStudentFromTeacherRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.TestService.GetStudentsByIdResponse.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("deleteStudentFromTeacher"))
              .build();
        }
      }
    }
    return getDeleteStudentFromTeacherMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static StudentsServiceStub newStub(io.grpc.Channel channel) {
    return new StudentsServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static StudentsServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new StudentsServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static StudentsServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new StudentsServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class StudentsServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getStudentById(com.example.grpc.TestService.GetStudentByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetStudentByIdMethod(), responseObserver);
    }

    /**
     */
    public void getAllStudents(com.example.grpc.TestService.GetAllStudentsRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllStudentsMethod(), responseObserver);
    }

    /**
     */
    public void createNewStudent(com.example.grpc.TestService.AddNewStudentRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateNewStudentMethod(), responseObserver);
    }

    /**
     */
    public void updateStudentById(com.example.grpc.TestService.UpdateStudentByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateStudentByIdMethod(), responseObserver);
    }

    /**
     */
    public void deleteStudentById(com.example.grpc.TestService.DeleteStudentByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteStudentByIdMethod(), responseObserver);
    }

    /**
     */
    public void getALlStudentsOfTeacher(com.example.grpc.TestService.GetStudentListOfTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentListOfTeacherResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetALlStudentsOfTeacherMethod(), responseObserver);
    }

    /**
     */
    public void attachStudentToTeacher(com.example.grpc.TestService.AttachStudentToTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getAttachStudentToTeacherMethod(), responseObserver);
    }

    /**
     */
    public void deleteStudentFromTeacher(com.example.grpc.TestService.DeleteStudentFromTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteStudentFromTeacherMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetStudentByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.GetStudentByIdRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_GET_STUDENT_BY_ID)))
          .addMethod(
            getGetAllStudentsMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.example.grpc.TestService.GetAllStudentsRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_GET_ALL_STUDENTS)))
          .addMethod(
            getCreateNewStudentMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.AddNewStudentRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_CREATE_NEW_STUDENT)))
          .addMethod(
            getUpdateStudentByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.UpdateStudentByIdRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_UPDATE_STUDENT_BY_ID)))
          .addMethod(
            getDeleteStudentByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.DeleteStudentByIdRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_DELETE_STUDENT_BY_ID)))
          .addMethod(
            getGetALlStudentsOfTeacherMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.example.grpc.TestService.GetStudentListOfTeacherRequest,
                com.example.grpc.TestService.GetStudentListOfTeacherResponse>(
                  this, METHODID_GET_ALL_STUDENTS_OF_TEACHER)))
          .addMethod(
            getAttachStudentToTeacherMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.AttachStudentToTeacherRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_ATTACH_STUDENT_TO_TEACHER)))
          .addMethod(
            getDeleteStudentFromTeacherMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.TestService.DeleteStudentFromTeacherRequest,
                com.example.grpc.TestService.GetStudentsByIdResponse>(
                  this, METHODID_DELETE_STUDENT_FROM_TEACHER)))
          .build();
    }
  }

  /**
   */
  public static final class StudentsServiceStub extends io.grpc.stub.AbstractStub<StudentsServiceStub> {
    private StudentsServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StudentsServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StudentsServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StudentsServiceStub(channel, callOptions);
    }

    /**
     */
    public void getStudentById(com.example.grpc.TestService.GetStudentByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetStudentByIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getAllStudents(com.example.grpc.TestService.GetAllStudentsRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllStudentsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createNewStudent(com.example.grpc.TestService.AddNewStudentRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateNewStudentMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateStudentById(com.example.grpc.TestService.UpdateStudentByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateStudentByIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteStudentById(com.example.grpc.TestService.DeleteStudentByIdRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteStudentByIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getALlStudentsOfTeacher(com.example.grpc.TestService.GetStudentListOfTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentListOfTeacherResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetALlStudentsOfTeacherMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void attachStudentToTeacher(com.example.grpc.TestService.AttachStudentToTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAttachStudentToTeacherMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteStudentFromTeacher(com.example.grpc.TestService.DeleteStudentFromTeacherRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteStudentFromTeacherMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class StudentsServiceBlockingStub extends io.grpc.stub.AbstractStub<StudentsServiceBlockingStub> {
    private StudentsServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StudentsServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StudentsServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StudentsServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.example.grpc.TestService.GetStudentsByIdResponse getStudentById(com.example.grpc.TestService.GetStudentByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetStudentByIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<com.example.grpc.TestService.GetStudentsByIdResponse> getAllStudents(
        com.example.grpc.TestService.GetAllStudentsRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllStudentsMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetStudentsByIdResponse createNewStudent(com.example.grpc.TestService.AddNewStudentRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateNewStudentMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetStudentsByIdResponse updateStudentById(com.example.grpc.TestService.UpdateStudentByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateStudentByIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetStudentsByIdResponse deleteStudentById(com.example.grpc.TestService.DeleteStudentByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getDeleteStudentByIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<com.example.grpc.TestService.GetStudentListOfTeacherResponse> getALlStudentsOfTeacher(
        com.example.grpc.TestService.GetStudentListOfTeacherRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetALlStudentsOfTeacherMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetStudentsByIdResponse attachStudentToTeacher(com.example.grpc.TestService.AttachStudentToTeacherRequest request) {
      return blockingUnaryCall(
          getChannel(), getAttachStudentToTeacherMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.TestService.GetStudentsByIdResponse deleteStudentFromTeacher(com.example.grpc.TestService.DeleteStudentFromTeacherRequest request) {
      return blockingUnaryCall(
          getChannel(), getDeleteStudentFromTeacherMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class StudentsServiceFutureStub extends io.grpc.stub.AbstractStub<StudentsServiceFutureStub> {
    private StudentsServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StudentsServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StudentsServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StudentsServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetStudentsByIdResponse> getStudentById(
        com.example.grpc.TestService.GetStudentByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetStudentByIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetStudentsByIdResponse> createNewStudent(
        com.example.grpc.TestService.AddNewStudentRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateNewStudentMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetStudentsByIdResponse> updateStudentById(
        com.example.grpc.TestService.UpdateStudentByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateStudentByIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetStudentsByIdResponse> deleteStudentById(
        com.example.grpc.TestService.DeleteStudentByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteStudentByIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetStudentsByIdResponse> attachStudentToTeacher(
        com.example.grpc.TestService.AttachStudentToTeacherRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getAttachStudentToTeacherMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.TestService.GetStudentsByIdResponse> deleteStudentFromTeacher(
        com.example.grpc.TestService.DeleteStudentFromTeacherRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteStudentFromTeacherMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_STUDENT_BY_ID = 0;
  private static final int METHODID_GET_ALL_STUDENTS = 1;
  private static final int METHODID_CREATE_NEW_STUDENT = 2;
  private static final int METHODID_UPDATE_STUDENT_BY_ID = 3;
  private static final int METHODID_DELETE_STUDENT_BY_ID = 4;
  private static final int METHODID_GET_ALL_STUDENTS_OF_TEACHER = 5;
  private static final int METHODID_ATTACH_STUDENT_TO_TEACHER = 6;
  private static final int METHODID_DELETE_STUDENT_FROM_TEACHER = 7;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final StudentsServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(StudentsServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_STUDENT_BY_ID:
          serviceImpl.getStudentById((com.example.grpc.TestService.GetStudentByIdRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        case METHODID_GET_ALL_STUDENTS:
          serviceImpl.getAllStudents((com.example.grpc.TestService.GetAllStudentsRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        case METHODID_CREATE_NEW_STUDENT:
          serviceImpl.createNewStudent((com.example.grpc.TestService.AddNewStudentRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        case METHODID_UPDATE_STUDENT_BY_ID:
          serviceImpl.updateStudentById((com.example.grpc.TestService.UpdateStudentByIdRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        case METHODID_DELETE_STUDENT_BY_ID:
          serviceImpl.deleteStudentById((com.example.grpc.TestService.DeleteStudentByIdRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        case METHODID_GET_ALL_STUDENTS_OF_TEACHER:
          serviceImpl.getALlStudentsOfTeacher((com.example.grpc.TestService.GetStudentListOfTeacherRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentListOfTeacherResponse>) responseObserver);
          break;
        case METHODID_ATTACH_STUDENT_TO_TEACHER:
          serviceImpl.attachStudentToTeacher((com.example.grpc.TestService.AttachStudentToTeacherRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        case METHODID_DELETE_STUDENT_FROM_TEACHER:
          serviceImpl.deleteStudentFromTeacher((com.example.grpc.TestService.DeleteStudentFromTeacherRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.TestService.GetStudentsByIdResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class StudentsServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    StudentsServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.grpc.TestService.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("StudentsService");
    }
  }

  private static final class StudentsServiceFileDescriptorSupplier
      extends StudentsServiceBaseDescriptorSupplier {
    StudentsServiceFileDescriptorSupplier() {}
  }

  private static final class StudentsServiceMethodDescriptorSupplier
      extends StudentsServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    StudentsServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (StudentsServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new StudentsServiceFileDescriptorSupplier())
              .addMethod(getGetStudentByIdMethod())
              .addMethod(getGetAllStudentsMethod())
              .addMethod(getCreateNewStudentMethod())
              .addMethod(getUpdateStudentByIdMethod())
              .addMethod(getDeleteStudentByIdMethod())
              .addMethod(getGetALlStudentsOfTeacherMethod())
              .addMethod(getAttachStudentToTeacherMethod())
              .addMethod(getDeleteStudentFromTeacherMethod())
              .build();
        }
      }
    }
    return result;
  }
}
