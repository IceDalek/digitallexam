package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class App {

    public static void thread(Runnable runnable, boolean daemon) {
        Thread brokerThread = new Thread(runnable);
        brokerThread.setDaemon(daemon);
        brokerThread.start();
    }

    public static void main(String[] args) {
        thread(new ServerInfoSubscriber(), false);
    }
}
