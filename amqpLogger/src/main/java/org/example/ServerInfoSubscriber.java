package org.example;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class ServerInfoSubscriber implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(
            App.class);

    @Override
    public void run() {

        try {
            ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");

            //Create Connection
            Connection connection = factory.createConnection();

            // Start the connection
            connection.start();

            // Create Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            //Create queue
            Destination queue = session.createQueue("Queue");

            MessageConsumer consumer = session.createConsumer(queue);
            while (true) {
                Message message = consumer.receive(1000);

                if (message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    String text = textMessage.getText();
                    System.out.println("Consumer Received: " + text);
                    logger.info(text);
                    if (text.equals("die")) {
                        session.close();
                        connection.close();
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Exception Occured");
            ex.printStackTrace();
        }
    }
}


